FROM ubuntu:16.04



# Install SteamCMD
RUN apt update && apt install curl perl-modules curl lsof libc6-i386 lib32gcc1 bzip2 --yes

RUN useradd -m steam
USER steam
RUN mkdir /home/steam/steamcmd
WORKDIR /home/steam/steamcmd
RUN curl -sqL "https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz" | tar zxvf -


# Install ark-server-tools
USER root
RUN curl -sL http://git.io/vtf5N | bash -s steam

USER steam
RUN arkmanager install --verbose



# Install ARK: Survival Evolved
#RUN mkdir /ark
#RUN /steam/steamcmd.sh +login anonymous +force_install_dir /ark +app_update 376030 +quit

# Run server
#CMD /ark/ShooterGame/Binaries/Linux/ShooterGameServer TheIsland?listen?SessionName=Jamestown?ServerPassword=james?ServerAdminPassword=jimw -server -log
